'use strict';

var jQuery = require('jquery');

window.$ = jQuery;
window.jQuery = jQuery;
global.$ = jQuery;
global.jQuery = jQuery;

module.exports = jQuery;
