from wagtail.tests.utils import WagtailPageTests
from .context import SETTINGS_KEYS
from django.conf import settings
from project.home.models import HomePage
from wagtail.core.models import Site, Page
from django.urls import reverse
from rest_framework.test import APIClient
from django.utils.text import slugify
from bs4 import BeautifulSoup


class BaseTestCase(WagtailPageTests):
    client = APIClient()

    def setUp(self):
        super().setUp()

        self.root = self.create_initial_homepage()

    def create_model(self, model, data={}, parent=None):
        parent = parent.pk if parent else self.root.pk
        add_url = reverse('wagtailadmin_pages:add', args=[
            model._meta.app_label, model._meta.model_name, parent
        ])
        data.update({
            'action-publish': 'action-publish',
            'body-count': 1,
            'body-0-deleted': '',
            'body-0-order': 0,
            'body-0-type': 'raw_html',
            'body-0-value': data['body'],
            'slug': slugify(data['title']),
            'subtitle': data.get('subtitle', data['title'] + ' subtitle')
        })
        return self.client.post(add_url, data)

    def create_test_user(self):
        self.user = super().create_test_user()
        return self.user

    def parse_content(self, content):
        return BeautifulSoup(content, 'html.parser')

    def get_html_title(self, response):
        return self.parse_content(response.content).title.string

    def create_initial_homepage(self):
        """
            from https://github.com/wagtail/wagtail/blob/master/wagtail/project_template/home/migrations/0002_create_homepage.py
        """
        Page.objects.filter(id=2).delete()
        HomePage.objects.all().delete()

        # Create a new homepage
        homepage = HomePage.objects.create(
            title="Homepage",
            subtitle="Homepage",
            body="Home Page",
            slug='home',
            path='00010001',
            depth=2,
            numchild=0,
            url_path='/',
        )
        Site.objects.create(hostname='localhost', root_page=homepage, is_default_site=True)
        return homepage


class ContextInjectorTestCase(BaseTestCase):
    def test_has_keys(self):
        response = self.client.get('/')
        for key in SETTINGS_KEYS:
            self.assertIn(key, response.context['settings'])
            self.assertEqual(response.context['settings'][key], getattr(settings, key))


class PageTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.create_model(HomePage, {
            'title': 'Test',
            'body': '<p>Some test content</p>',
        })
        self.page = HomePage.objects.get(title='Test')

    def test_short_body(self):
        self.assertEqual(self.page.get_short_body(), 'Some test content')

    def test_page(self):
        response = self.client.get(self.page.url)
        self.assertEqual(response.status_code, 200)
        content = self.parse_content(response.content)
        self.assertIn(self.page.get_short_body(), content.get_text())

    def test_page_title(self):
        response = self.client.get(self.page.url)
        self.assertEqual(response.status_code, 200)
        title = self.get_html_title(response)
        self.assertEqual(self.page.get_page_title(), title)
        self.assertEqual(self.page.get_page_title(), "Test :: {}".format(settings.WAGTAIL_SITE_NAME))

    def test_nested_page_title(self):
        self.create_model(HomePage, {
            'title': 'Test Child',
            'body': '<p>Some content of the child page</p>',
        }, parent=self.page)
        new_page = HomePage.objects.get(title='Test Child')
        response = self.client.get(new_page.url)
        self.assertEqual(response.status_code, 200)
        title = self.get_html_title(response)
        self.assertEqual(new_page.get_page_title(), title)
        self.assertEqual(new_page.get_page_title(), "Test Child :: Test :: {}".format(settings.WAGTAIL_SITE_NAME))
