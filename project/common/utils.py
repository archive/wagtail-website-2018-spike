from project.home.models import HomePage


def get_page_title(page):
    title = "{} :: ".format(page.title)
    homepage = HomePage.objects.live().first()
    parent = page.get_parent()
    if parent and parent.url and parent.id != homepage.id:
        title += get_page_title(parent)
    return title
