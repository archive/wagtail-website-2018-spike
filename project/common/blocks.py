from wagtail.core.fields import StreamField
from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.embeds.blocks import EmbedBlock
from wagtailmarkdown.blocks import MarkdownBlock

HEADING_CHOICES = [('h' + str(i), 'H' + str(i)) for i in range(1, 6)]


class HeadingBlock(blocks.StructBlock):
    size = blocks.ChoiceBlock(choices=HEADING_CHOICES)
    value = blocks.CharBlock()

    class Meta:
        icon = 'title'
        template = 'blocks/heading.html'


class VideoBlock(blocks.StructBlock):
    video = EmbedBlock()
    caption = blocks.CharBlock()

    class Meta:
        template = 'blocks/video.html'


def build_stream_field():
    return StreamField([
        ('document', DocumentChooserBlock()),
        ('heading', HeadingBlock()),
        ('image', ImageChooserBlock()),
        ('markdown', MarkdownBlock()),
        ('ol', blocks.ListBlock(blocks.CharBlock(label="List Item"), icon="list-ol", label="Ordered List", template='blocks/ordered-list.html')),
        ('paragraph', blocks.RichTextBlock()),
        ('raw_html', blocks.RawHTMLBlock(label="Raw HTML")),
        ('ul', blocks.ListBlock(blocks.CharBlock(label="List Item"), icon="list-ul", label="Unordered List")),
        ('video', VideoBlock())
    ])
