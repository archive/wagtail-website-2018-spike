from apscheduler.schedulers.blocking import BlockingScheduler
from django.core.management.base import BaseCommand
from django.core.management import call_command
import logging
import functools


logger = logging.getLogger(__file__)


class Command(BaseCommand):

    @classmethod
    def call_management_command(cls, command_name):
        wrapped = functools.partial(call_command, command_name)
        wrapped.__qualname__ = command_name
        return wrapped

    def handle(self, **options):
        scheduler = BlockingScheduler()
        scheduler.add_job(self.call_management_command("publish_scheduled_pages"), 'cron', minute=0)
        scheduler.add_job(self.call_management_command("fixtree"), 'cron', day_of_week=0)
        scheduler.add_job(self.call_management_command("update_index"), 'cron', day_of_week=0)
        scheduler.add_job(self.call_management_command("search_garbage_collect"), 'cron', day_of_week=0)
        scheduler.start()
