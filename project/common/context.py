from django.conf import settings
from project.home.models import HomePage


SETTINGS_KEYS = [
    'BASE_URL',
    'STATIC_URL',
    'MEDIA_URL',
    'LANGUAGE_CODE',
    'TIME_ZONE',
    'ALLOWED_HOSTS',
    'WAGTAIL_SITE_NAME'
]


def settings_injector(request):
    return {'settings': {
        setting: getattr(settings, setting) for setting in SETTINGS_KEYS
    }}


def get_home_page(request):
    return {
        'homepage': HomePage.objects.live().first()
    }
