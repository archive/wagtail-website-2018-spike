from wagtail.admin.edit_handlers import FieldPanel
from project.common.models import Entity
from django.db import models


class HomePage(Entity):
    is_home = True

    body = models.CharField(max_length=255)

    content_panels = Entity.content_panels + [
        FieldPanel('body', classname="full")
    ]
