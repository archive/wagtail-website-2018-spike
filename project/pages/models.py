from django.db import models
from project.common.blocks import build_stream_field
from project.common.models import Entity
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel


class SimpleContentPage(Entity):
    body = build_stream_field()

    content_panels = Entity.content_panels + [
        StreamFieldPanel('body')
    ]


class SectionIndexPage(Entity):
    body = build_stream_field()
    hide_list = models.BooleanField(default=False)

    content_panels = Entity.content_panels + [
        StreamFieldPanel('body'),
        FieldPanel('hide_list')
    ]
