from project.common.blocks import build_stream_field
from wagtail.admin.edit_handlers import StreamFieldPanel, FieldPanel
from wagtail.search import index
from project.common.models import Entity
from django.db import models
from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey(
        'BlogPage',
        related_name='tagged_items',
        on_delete=models.CASCADE
    )


class BlogPage(Entity):
    body = build_stream_field()

    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)

    search_fields = Entity.search_fields + [
        index.SearchField('body'),
    ]

    content_panels = Entity.content_panels + [
        StreamFieldPanel('body'),
        FieldPanel('tags')
    ]

    class Meta:
        ordering = ('post_date',)

    def get_date_group(self):
        if self.post_date:
            return self.post_date.strftime("%Y-%m")


class BlogIndexPage(Entity):
    body = build_stream_field()

    subpage_types = [BlogPage]

    content_panels = Entity.content_panels + [
        StreamFieldPanel('body')
    ]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request)
        child_pages = self.get_children().specific().live().values_list('id', flat=True)
        context['blogs'] = BlogPage.objects.filter(id__in=child_pages)
        tag_filter = request.GET.get('tag')
        if tag_filter:
            context['tag'] = tag_filter
            context['blogs'] = context['blogs'].filter(tags__name=tag_filter)
        return context
