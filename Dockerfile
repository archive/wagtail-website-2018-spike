FROM ubuntu:18.04

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

RUN mkdir /app

RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    nodejs \
    npm \
    git

RUN pip3 install pipenv

WORKDIR /app

COPY Pipfile Pipfile
COPY Pipfile.lock Pipfile.lock
RUN pipenv install --deploy --system --dev

COPY package.json package.json
COPY package-lock.json package-lock.json
RUN npm install

COPY manage.py /app/manage.py
COPY project/ /app/project/
COPY static/ /app/static/
COPY scripts/ /app/scripts/
COPY templates/ /app/templates/
COPY .sass-lint.yml /app/.sass-lint.yml
COPY .eslintrc /app/.eslintrc
COPY Procfile /app/Procfile

RUN BASE_URL= ./scripts/build-static.sh

EXPOSE 8000
