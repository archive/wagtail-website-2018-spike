BASEDIR=$PWD
NODE_BIN=$BASEDIR/node_modules/.bin

STATIC_SRC=$BASEDIR/static/src
STATIC_BUILD=$BASEDIR/static/build

export PATH=$NODE_BIN:$BASEDIR/bin:$PATH
