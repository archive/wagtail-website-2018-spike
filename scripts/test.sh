#!/usr/bin/env bash

set -e

source ./scripts/common.sh

export IN_TEST=true

echo ">> Running flake8..."
flake8 project --ignore=E128,E501 --exclude=migrations,wsgi.py

echo ">> Running backend unit tests..."
coverage run --source=project --omit='*/wsgi.py,*/settings.py,*/migrations/*.py,*__init__.py' manage.py test $@
coverage report

echo ">> Linting SCSS..."
sass-lint -vqc .sass-lint.yml

echo ">> Linting JS..."
eslint $STATIC_SRC/js
