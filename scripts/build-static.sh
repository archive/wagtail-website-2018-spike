#!/usr/bin/env bash

set -e

source ./scripts/common.sh

rm -rf $STATIC_BUILD
mkdir -p $STATIC_BUILD/js $STATIC_BUILD/css

cp -r $BASEDIR/node_modules/lightgallery/dist/fonts $STATIC_BUILD
cp -r $STATIC_SRC/img $STATIC_BUILD/img
cp -r $BASEDIR/node_modules/lightgallery/dist/img/* $STATIC_BUILD/img

parcel build $STATIC_SRC/js/index.js -d $STATIC_BUILD/js/ -o app.js
node-sass $STATIC_SRC/scss/style.scss $STATIC_BUILD/css/style.css --source-map-embed

cp $BASEDIR/node_modules/mermaid/dist/mermaid.min.js $STATIC_BUILD/js/mermaid.min.js

./manage.py collectstatic --noinput
